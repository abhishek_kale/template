from django.shortcuts import render

# Create your views here.
def index(request):
    context = {"report":"is-active"}
    return render(request,'index.html',context)

def login(request):
    return render(request,'login.html')

def report(request):
    return render(request,'report.html')


def deliver2C(request):
    context = {"companies":"is-active"}
    return render(request,'orders/deliver2C.html',context)

def orderFC(request):
    context = {"companies":"is-active"}
    return render(request,'orders/orderFC.html',context)

def deliverFF(request):
    context = {"companies":"is-active"}
    return render(request,'orders/deliverFF.html',context)

def order2F(request):
    context = {"companies":"is-active"}
    return render(request,'orders/order2F.html',context)

def bill(request):
    context = {"companies":"is-active"}
    return render(request,'bill/bill.html',context)

def reciept(request):
    context = {"companies":"is-active"}
    return render(request,'bill/reciept.html',context)
def pricing(request):
    context = {"companies":"is-active"}
    return render(request,'bill/pricing.html',context)
def addexpense(request):
    context = {"companies":"is-active"}
    return render(request,'expenses/addexpense.html',context)
def seereport(request):
    context = {"companies":"is-active"}
    return render(request,'specific/report.html',context)

    