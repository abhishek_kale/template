from django.urls import path
from . import views
urlpatterns = [
    path('',views.index,name="index"),
    path('login/',views.login,name="login"),
    path('report/',views.report,name="report"),
    path('ofc/',views.orderFC,name="ofc"),
    path('dff/',views.deliverFF,name="dff"),
    path('otf/',views.order2F,name="otf"),
    path('dtc/',views.deliver2C,name="dtc"),
    path('bill/',views.bill,name="bill"),
    path('reciept/',views.reciept,name="reciept"),
    path('pricing/',views.pricing,name="pricing"),
    path('addexpense/',views.addexpense,name="addexpense"),
    path('seereport/',views.seereport,name="seereport"),
 
 ]