const data = {
    daily:{
        labels: ["8 am","12 pm" ,"4 pm","8 pm" ,"12 am"],
        values:[4,  3, 15, 2,7]
    },
}
//daily 
var dailyCtx = document.getElementById('Chart').getContext('2d');
//render chart
var dailyChart = new Chart(dailyCtx, {
    type: 'line',
    data: {
        labels: data.daily.labels,
        datasets: [{
            label: 'Sales in thousand',
            data: data.daily.values,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});