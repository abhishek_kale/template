
const data = {
    daily:{
        labels: ["8 am","12 pm" ,"4 pm","8 pm" ,"12 am"],
        values:[4,  3, 15, 2,7]
    },
    weekly:{
        labels:['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat','Sun'],
        values:[12, 19, 3, 5, 2, 3,7],
    },
    monthly:{
        labels:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
        values:[12, 19, 3, 5, 2, 3,7,12, 19, 3, 5, 2, 3,7,12, 19, 3, 5, 2, 3,7,12, 19, 3, 5, 2, 3,7,2,11],
    },
    expenses:{
        labels:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
        values:[12, 19, 3, 5, 2, 3,7,12, 19, 3, 5, 2, 3,7,12, 19, 3, 5, 2, 3,7,12, 19, 3, 5, 2, 3,7,2,11],
    }
}

//daily 
var dailyCtx = document.getElementById('daily').getContext('2d');
//render chart
var dailyChart = new Chart(dailyCtx, {
    type: 'line',
    data: {
        labels: data.daily.labels,
        datasets: [{
            label: 'Sales in thousand',
            data: data.daily.values,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
//weekly chart
var weeklyCtx = document.getElementById('weekly').getContext('2d');
//render chart
var weeklyChart = new Chart(weeklyCtx, {
    type: 'line',
    data: {
        labels: data.weekly.labels,
        datasets: [{
            label: 'Sales in thousand',
            data: data.weekly.values,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

//Monthly 
var monthlyCtx = document.getElementById('monthly').getContext('2d');
//render chart
var monthlyChart = new Chart(monthlyCtx, {
    type: 'line',
    data: {
        labels: data.monthly.labels,
        datasets: [{
            label: 'Sales in thousand',
            data:data.monthly.values,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

//Monthly 
var expenseCtx = document.getElementById('expenses').getContext('2d');
//render chart
var expenseChart = new Chart(expenseCtx, {
    type: 'bar',
    data: {
        labels: data.expenses.labels,
        datasets: [{
            label: 'overall expenditure',
            data:data.expenses.values,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
